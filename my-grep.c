/*Käyttöjärjestelmät ja systeemiohjelmointi
Harjoitustyöprojekti 1 my-grep
Susanna Järventausta 0542325*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void searchStdin(char *files[]);
void searchFiles(int amount, char *files[]);

int main(int amount, char *files[]){

	//If no searchterm or files given
	if (amount == 1) {
   		printf("my-grep: searchterm [file ...]\n");
    		exit(1);

	//If no files given
	}else if (amount ==2){
		searchStdin(files);

	//If searchterm and file/files given
	}else{
		searchFiles(amount, files);
	}
	return 0;
}

void searchStdin(char *files[]) {
	char *line_buf = NULL;
	size_t line_buf_size = 0;
	ssize_t line_size;
	
	//Get the first line of stdin
	line_size = getline(&line_buf, &line_buf_size, stdin);

	//Go trough all the lines in stdin
	while(line_size > 0){

		//Print the line if line_buf contains the given commandline parameter
		if(strstr(line_buf, files[1]) != NULL){
				printf("%s", line_buf);}

		//Get the next line of stdin
		line_size = getline(&line_buf, &line_buf_size, stdin);
	}
}

void searchFiles(int amount, char *files[]){
	char *line_buf = NULL;
	size_t line_buf_size = 0;
	ssize_t line_size;

	//Go through all given files
	for (int i=2; i<amount; i++) {
		
		//Open file and show error and exit if couldn't open
		FILE *file = fopen(files[i], "r");
		if (file == NULL) {
   			printf("my-grep: cannot open file\n");
    			exit(1);}

		printf("\n\n\nSearching %s:\n\n", files[i]);

		//Get the first line of the file
		line_size = getline(&line_buf, &line_buf_size, file);

		//Go trough all the lines in the file
		while(line_size > 0){

			//Print the line if line_buf contains the given commandline parameter
			if(strstr(line_buf, files[1]) != NULL){
				printf("%s", line_buf);}

			//Get the next line of the file
			line_size = getline(&line_buf, &line_buf_size, file);
		}
		fclose(file);
	}
}
