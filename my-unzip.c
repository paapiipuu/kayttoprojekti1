/*Käyttöjärjestelmät ja systeemiohjelmointi
Harjoitustyöprojekti 1 my-zip
Susanna Järventausta 0542325*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void unzip(char *fileName);

int main(int amount, char *files[]){

	//If no input or output files given
	if (amount == 1) {
   		printf("my-unzip: file1 [file2 ...]\n");
    		exit(1);}

	for (int i = 1; i<amount; i++){
		unzip(files[i]);}

	printf("\n");
	return 0;
}

void unzip(char *fileName){
	FILE *file;
	char character;
	int number;

	//Open zipped file
	file = fopen(fileName, "r");
	if (file == NULL) {
   		printf("my-unzip: cannot open file\n");
    		exit(1);}
	
	//Read the next 4 byte number from the file
	while(fread(&number, 4, 1, file) == 1){

		fread(&character, 1, 1, file);
		//Print the character for the number of times
		for(int i = 0; i<number; i++){
			printf("%c", character);}
	}

	fclose(file);
}

