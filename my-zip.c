/*Käyttöjärjestelmät ja systeemiohjelmointi
Harjoitustyöprojekti 1 my-zip
Susanna Järventausta 0542325*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void zipToStdout(char *fileName);

int main(int amount, char *files[]){

	//If no input files given
	if (amount == 1) {
   		printf("my-zip: file1 [file2 ...]\n");
    		exit(1);}

	//Go through all the input files
	for (int i = 1; i<amount; i++){
		zipToStdout(files[i]);}

	return 0;
}


void zipToStdout(char *fileName){
	FILE *file;
	char charBefore, charNow;
	int count = 0; //Counts the amount of the same words

	//Open file
	file = fopen(fileName, "r");
	if (file == NULL) {
   		printf("my-zip: cannot open file\n");
    		exit(1);}
	
	//If the input file isn't empty
	if ((charBefore = fgetc(file)) != EOF){

		count = 1;
		//Loops throught the file, reading one character at a time
		//Same characters increase count
		//When a different character is reached the same ones before that are printed
		while ((charNow = fgetc(file)) != EOF){

			if(charNow == charBefore){
				count = count + 1;
			}else{

				if(fwrite(&count, sizeof(int), 1 , stdout) != 1){
					perror("my-zip: fwrite not working\n");
					exit(1);}

				if(fwrite(&charBefore, sizeof(char), 1, stdout) != 1) {
					perror("my-zip: write not working\n");
					exit(1);}
				count = 1;
			}
			charBefore = charNow;
		}
	}
	fclose(file);
}


