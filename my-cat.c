/*Käyttöjärjestelmät ja systeemiohjelmointi
Harjoitustyöprojekti 1 my-cat
Susanna JärventaustaS 0542325*/

#include <stdio.h>
#include <stdlib.h>

#define max 100

int main(int amount, char *files[]){
	char line[max];

	//No files given
	if(amount == 1){
		exit(0);}

	//Go through all given files
	for (int i=1; i<amount; i++) {

		//Open file, show error and exit if couldn't open		
		FILE *file = fopen(files[i], "r");
		if (file == NULL) {
   			printf("my-cat: cannot open file\n");
    			exit(1);}

		printf("\n\n\nContents of %s:\n\n", files[i]);

		//Go through all the lines and print them
		while(fgets(line, max, file) != NULL){
			printf("%s", line);
		}
		fclose(file);
	}
	return 0;
}
